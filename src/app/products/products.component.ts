import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { AppState } from '../app.reducer';
import { Product } from '../models/product.model';
import * as productActions from './products.actions';
import { Cart } from '../models/cart.model';
import { ProductCart } from '../models/product-cart.model';
import { ProductCartsService } from '../services/product-carts.service';


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit,OnDestroy {

  displayedColumns: string[] = ['name', 'price', 'description','cart'];
  
  products:Product[]=[]

  selectedProducts:Product[]=[]

  productCarts:ProductCart[]=[];

  cart!:Cart

  selectedSubs!:Subscription
  productsSubs!:Subscription

  constructor(private store:Store<AppState>,private productCartService:ProductCartsService) { }
  ngOnDestroy(): void {
    this.selectedSubs.unsubscribe();
    this.productsSubs.unsubscribe();
  }

  ngOnInit(): void {
   this.productsSubs= this.store.select('products')
    .subscribe(({products}) => this.products=products)

    this.store.select('carts')
    .subscribe(({carts}) => {
      this.cart = carts.find(cart => cart.status === 'pending') as Cart || null;
       console.log(this.cart)
    })

   this.selectedSubs= this.store.select('products').subscribe(({selectedProducts}) => this.selectedProducts=selectedProducts)

   this.store.select('productCarts').subscribe(({productCarts}) => this.productCarts = productCarts)
  }

  async selectProduct(product:Product){
    let exist = this.productCarts.some(item => item.product_id === product.id)
    if(!exist){
      const newProductCart = new ProductCart(product.id,this.cart.id!,1,product,this.cart);
      await this.productCartService.createProductCart(newProductCart)
    }
  }

  

}
