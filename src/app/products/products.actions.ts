import { createAction, props } from '@ngrx/store';
import { Product } from '../models/product.model';

export const setProducts = createAction(
    '[Products] setProducts',
    props<{products: Product[]}>()
);


export const unSetProducts = createAction(
    '[Products] unSetProducts'
);

export const addSelectedProduct = createAction(
    '[Products] addSelectedProduct',
    props<{product:Product}>()
)



