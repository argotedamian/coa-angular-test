import { createReducer, on } from '@ngrx/store';
import * as products from './products.actions';
import { Product } from '../models/product.model';

export interface State {
    products:Product[],
    selectedProducts:Product[],   
};

const initialState: State = {
        products:[],
        selectedProducts:[]
};

const _productReducer = createReducer(initialState,

    on(products.setProducts,(state,{products})=> ({...state,products:[...products]})),
    on(products.unSetProducts,state => ({...state,products:[]})),
    on(products.addSelectedProduct,((state,{product}) => ({...state,selectedProducts:[...state.selectedProducts,product]})))

)


export function productReducer(state:any,action:any){
    return _productReducer(state,action)
}