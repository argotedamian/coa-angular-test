import { Product } from './product.model';
import { Cart } from './cart.model';


export class ProductCart{

    constructor(
        public product_id:string,
        public cart_id:string,
        public quantity:number,
        public product?:Product,
        public cart?:Cart,
        public id?:string,
    ){}
}