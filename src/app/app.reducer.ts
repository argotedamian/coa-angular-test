import {ActionReducerMap} from '@ngrx/store'
import * as ui from './shared/ui.reducer';
import * as auth from './auth/auth.reducer';
import * as products from './products/products.reducer'
import * as carts from './carts/cart.reducer'
import * as productCarts from './carts/product-cart.reducer';

export interface AppState{
    ui:ui.State,
    user:auth.State,
    products:products.State,
    carts:carts.State,
    productCarts:productCarts.State
}


export const appReducers:ActionReducerMap<AppState> = {
    ui:ui.uiReducer,
    user:auth.authReducer,
    products:products.productReducer,
    carts:carts.cartReducer,
    productCarts:productCarts.productCartReducer
}
