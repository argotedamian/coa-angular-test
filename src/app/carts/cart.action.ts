import { createAction, props } from '@ngrx/store';
import { Cart } from '../models/cart.model';

export const setCarts = createAction(
    '[Cart] setCarts',
    props<{carts:Cart[]}>()
);

export const unSetCart = createAction(
    '[Cart] unSetCarts'
);
