import { createReducer, on } from '@ngrx/store';
import { ProductCart } from '../models/product-cart.model';
import * as productCarts from './product-cart.actions';


export interface State {
    productCarts:ProductCart[];
 };


 const initialState:State = {
    productCarts:[]
};


const _productCartReducer = createReducer(initialState,

    on(productCarts.setProductCarts,(state,{productCarts}) => ({...state,productCarts:[...productCarts]})),
    on(productCarts.unSetProductCarts,state => ({...state,productCarts:[]}))

);


export function productCartReducer(state: any, action: any) {
    return _productCartReducer(state, action);
}