import { createAction, props } from '@ngrx/store';
import { ProductCart } from '../models/product-cart.model';


export const setProductCarts = createAction(
    '[Product-Cart] setProductCarts',
    props<{productCarts:ProductCart[]}>()
);

export const unSetProductCarts = createAction(
    '[Product-Cart] unSetProductCarts'
);


