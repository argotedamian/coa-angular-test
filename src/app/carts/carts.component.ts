import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { AppState } from '../app.reducer';
import { ProductCart } from '../models/product-cart.model';
import { ProductCartsService } from '../services/product-carts.service';
import { CartsService } from '../services/carts.service';
import { Cart } from '../models/cart.model';

@Component({
  selector: 'app-carts',
  templateUrl: './carts.component.html',
  styleUrls: ['./carts.component.css']
})
export class CartsComponent implements OnInit,OnDestroy {

  displayedColumns: string[] = ['name', 'price','quantity','actions'];
  productCarts:ProductCart[]=[]
  productCartSubs!:Subscription

  disabled:boolean=false

  cart!:Cart 

  constructor(private store:Store<AppState>,private productCartsService:ProductCartsService,
              private cartsService:CartsService) { }

  ngOnDestroy(): void {
    this.productCartSubs.unsubscribe()
  }

  ngOnInit(): void {

  this.productCartSubs= this.store.select('productCarts')
   .subscribe(({productCarts}) => {
     this.productCarts = productCarts
     this.productCarts.length === 0 ? this.disabled=true : this.disabled=false
   })

   this.store.select('carts')
   .subscribe(({carts}) =>{
     this.cart = carts.find(cart => cart.status === 'pending') as Cart || null;
   })
  }

 async removeCart(id:string){
   await this.productCartsService.deleteProductCart(id)
  }

  async sumQuantity(id:string, quantity:number){
    let sum = quantity + 1
   await this.productCartsService.editProductCart(id,sum)
  }

  async subtractQuantity(id:string, quantity:number){
    let sub = quantity - 1
    if(sub < 1) return
   await this.productCartsService.editProductCart(id,sub)
  }

  async completeCart(){

    if(!this.disabled){
      const newCart = new Cart('pending')
      await this.cartsService.editCart(this.cart.id!,'completed')
      await this.cartsService.newCart(newCart)
      this.productCartsService.deleteAllProductCart(this.productCarts)
    }
   
  }
}
