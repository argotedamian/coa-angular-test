import { createReducer, on } from '@ngrx/store';
import * as cartAction from './cart.action';
import { Cart } from '../models/cart.model';


export interface State {
   carts:Cart[];
};

const initialState:State = {
    carts:[]
};

const _cartReducer = createReducer(
    initialState,
   on(cartAction.setCarts,(state,{carts}) => ({...state,carts:[...carts]})),
   on(cartAction.unSetCart,state => ({...state,carts:[]}))
);

export function cartReducer(state: any, action: any) {
    return _cartReducer(state, action);
}