import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AuthService } from '../../services/auth.service';
import { AppState } from '../../app.reducer';


interface Item{
  item:string;
  link:string;
  icon:string;
}

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  showFiller = true;
  user:any

  items:Item[]=[
    {
      item:'Homer',
      link:'',
      icon:'home'
    },
    {
      item:'Shopping-cart',
      link:'/cart',
      icon:'shopping_cart'
    }
  ];

  constructor(private authService:AuthService,private router:Router, private store:Store<AppState>) { }

  ngOnInit(): void {

    this.store.select('user').subscribe(({user}) => this.user = user || '')

  }

  logout(){
    this.authService.logout().then(() => this.router.navigateByUrl('/login'))
  }

}
