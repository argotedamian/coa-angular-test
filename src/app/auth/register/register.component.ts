import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { AuthService } from '../../services/auth.service';
import { AppState } from '../../app.reducer';
import * as ui from '../../shared/ui.actions';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit,OnDestroy {

  registerForm!:FormGroup;
  loading:boolean=false;
  uiSubs!:Subscription
  msg!:string

  constructor(private fb:FormBuilder, 
              private authService:AuthService,
              private router:Router,
              private store:Store<AppState>) { }
              
  ngOnDestroy(): void {
    this.uiSubs.unsubscribe();
  }

  ngOnInit(): void {

    this.registerForm= this.fb.group({
      name:['',Validators.required],
      email:['',[Validators.required,Validators.email]],
      password:['',[Validators.required,Validators.minLength(6)]]
    })

    this.uiSubs= this.store.select('ui').subscribe(ui => this.loading = ui.isLoading)
  }

  registerSubmit(){

    if(this.registerForm.invalid){return}
    this.store.dispatch(ui.isLoading());
    const {name,email,password}=this.registerForm.value;

    this.authService.registerUser(name,email,password)
    .then(() => {
      this.store.dispatch(ui.stopLoading())
      this.router.navigateByUrl('')
    })
    .catch(err =>{
      this.msg=err.message
      this.store.dispatch(ui.stopLoading())
    })
  }

}
