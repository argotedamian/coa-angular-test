import { createReducer, on } from '@ngrx/store';
import * as authActions from './auth.actions';
import { User } from '../models/user.model';

export interface State {
   user:User | null      
};

const initialState: State = {
    user:null  
};

const _authReducer = createReducer(initialState,
    on(authActions.setUser, (state,{user}) => ({...state,user})),
    on(authActions.unSetUser,state => ({...state,user:null}))
);


export function authReducer(state:any,action:any){
    return _authReducer(state,action);
}