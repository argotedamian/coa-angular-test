import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.reducer';
import * as ui from '../../shared/ui.actions';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit,OnDestroy {

  loginForm!:FormGroup;
  loading:boolean=false;
  uiSubs!:Subscription
  msg!:string

  constructor(private fb:FormBuilder, 
              private authService:AuthService,
              private router:Router,
              private store:Store<AppState>) { }

  ngOnDestroy(): void {
    this.uiSubs.unsubscribe();
  }

  ngOnInit(): void {

    this.loginForm= this.fb.group({
      email:['',[Validators.required,Validators.email]],
      password:['',[Validators.required,Validators.minLength(6)]]
    })

    this.uiSubs= this.store.select('ui').subscribe(ui => this.loading = ui.isLoading)
  }


  loginSubmit(){
    if(this.loginForm.invalid){return}
    this.store.dispatch(ui.isLoading());

    const{email,password}=this.loginForm.value;

    this.authService.loginUser(email,password)
    .then( () =>{
      this.store.dispatch(ui.stopLoading())
      this.router.navigateByUrl('');
    })
    .catch(err =>{
      this.store.dispatch(ui.stopLoading())
      
      this.msg=err.message
    })
  }

}
