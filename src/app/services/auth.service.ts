import { Injectable } from '@angular/core';
import 'firebase/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import {map} from 'rxjs/operators'
import { User } from '../models/user.model';
import { Store } from '@ngrx/store';
import { AppState } from '../app.reducer';
import * as authActions from '../auth/auth.actions';
import { Subscription } from 'rxjs';
import { unSetProducts } from '../products/products.actions';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  userSubs!:Subscription

  private _user:any;

  get user():any{
    return {...this.user};
  }

  constructor(public auth: AngularFireAuth,
              private firestore: AngularFirestore,
              private store:Store<AppState>) { }

  initAuthListener(){
    this.auth.authState.subscribe( fuser => {

      if(fuser){
       this.userSubs= this.firestore.doc(`${fuser.uid}/user`).valueChanges()
        .subscribe(firestoreUser =>{
          const user = User.fromFirebase(firestoreUser)
          this._user = user;
          this.store.dispatch(authActions.setUser({user}))
        })
        
      }else{
        this._user=null
        this.userSubs?.unsubscribe();
        this.store.dispatch(authActions.unSetUser())
        this.store.dispatch(unSetProducts())
      }

      
    })
  }

  registerUser(name:string,email:string,password:string){
    return this.auth.createUserWithEmailAndPassword(email,password)
          .then(({user}) => {

            const newUser = new User(user?.uid!,name,email);
            return this.firestore.doc(`${user?.uid}/user`).set({...newUser})
            
          })

  }

  loginUser(email:string,password:string){
    return this.auth.signInWithEmailAndPassword(email,password);
  }

  logout(){
    
    return this.auth.signOut()
    
  }

  isAuth(){
    return this.auth.authState.pipe(
      map( fbUser => fbUser != null)
    );
  }


}
