import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { ProductCart } from '../models/product-cart.model';

@Injectable({
  providedIn: 'root'
})
export class ProductCartsService {

  constructor(private firestore: AngularFirestore,private router:Router) { }

  initProductCartsListener(){
    return this.firestore.collection('product_carts')
     .snapshotChanges()
     .pipe(
       map(snapshot => {
         return snapshot.map(doc =>{
           return{
             id:doc.payload.doc.id,
             ...doc.payload.doc.data() as any
           }
         })
       })
     );
   }

  createProductCart(productCart:ProductCart){
    let newProductCart = {...productCart}
    delete newProductCart.id
    return this.firestore.collection('product_carts').add(newProductCart)
  }


  deleteProductCart(id:string){
    return this.firestore.doc(`/product_carts/${id}`).delete();
  }

  editProductCart(id:string,quantity:number){
    return this.firestore.doc(`/product_carts/${id}`).update({quantity});
  }

  deleteAllProductCart(productCarts:ProductCart[]){

    productCarts.forEach( productCart =>{
      console.log(productCart.id)
      this.firestore.doc(`/product_carts/${productCart.id}`).delete()
      .then(() => console.log('removed'))
      .catch(err => console.log(err))
    })

    this.router.navigateByUrl('')
  }

}
