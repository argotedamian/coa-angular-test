import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import {map} from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private firestore: AngularFirestore) { }

  initProductsListener(){
   return this.firestore.collection('products')
    .snapshotChanges()
    .pipe(
      map(snapshot => {
        return snapshot.map(doc =>{
          
          return{
            id:doc.payload.doc.id,
            ...doc.payload.doc.data() as any
          }
        })
      })
    );
  }

}
