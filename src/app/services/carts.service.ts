import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { Cart } from '../models/cart.model';

@Injectable({
  providedIn: 'root'
})
export class CartsService {

  constructor(private firestore: AngularFirestore) { }


  newCart(cart:Cart){
    let newCart = {...cart}
    delete newCart.id
    return this.firestore.collection('carts').add(newCart)
  }

  initCartsListener(){
    return this.firestore.collection('carts')
     .snapshotChanges()
     .pipe(
       map(snapshot => {
         return snapshot.map(doc =>{
           
           return{
             id:doc.payload.doc.id,
             ...doc.payload.doc.data() as any
           }
         })
       })
     );
   }


   editCart(id:string,status:string){
    return this.firestore.doc(`/carts/${id}`).update({status})
                
  }

}
