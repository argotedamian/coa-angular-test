import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { AppState } from '../app.reducer';
import { CartsService } from '../services/carts.service';
import { ProductsService } from '../services/products.service';
import * as productActions from '../products/products.actions';
import * as cartActions from '../carts/cart.action';
import { Cart } from '../models/cart.model';
import { ProductCartsService } from '../services/product-carts.service';
import * as productCartActions from '../carts/product-cart.actions';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit,OnDestroy {

  userSubs!:Subscription

  constructor(private store:Store<AppState>,
              private productsService:ProductsService,
              private cartsService:CartsService,
              private productCartService:ProductCartsService) { }
  ngOnDestroy(): void {
    this.userSubs.unsubscribe()
  }

  ngOnInit(): void {

    this.userSubs= this.store.select('user')
    .pipe(
      filter(auth => auth.user !== null)
    )
    .subscribe( user => {
      console.log(user)
    })

    this.productsService.initProductsListener()
    .subscribe(products =>{
      console.log(products)
      this.store.dispatch(productActions.setProducts({products:products}))
    })

    this.cartsService.initCartsListener()
    .subscribe( carts => {
      this.store.dispatch(cartActions.setCarts({carts:carts}))
    })

    this.productCartService.initProductCartsListener()
    .subscribe(productCarts =>{
      this.store.dispatch(productCartActions.setProductCarts({productCarts}))
    })

  }

}
