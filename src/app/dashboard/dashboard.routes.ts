import { Routes } from '@angular/router';
import { ProductsComponent } from '../products/products.component';
import { CartsComponent } from '../carts/carts.component';


export const dashboardRoutes:Routes=[

    {path:'',component:ProductsComponent},
    {path:'cart',component:CartsComponent}

];